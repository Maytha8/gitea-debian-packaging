# Gitea Debian Packaging

Here is where I'm tracking the progress of getting Gitea and all its
dependencies into Debian.

For the actual packaging, it's located the [Debian Go Packaging
Team](https://salsa.debian.org/go-team/packages).

## Dependency Graph

![Gitea dependency graph](./gitea.gv.svg)

Download PDF
[here](https://salsa.debian.org/Maytha8/gitea-debian-packaging/-/raw/main/gitea.gv.pdf?inline=false).

Generated using [gophian](https://codeberg.org/Maytha8/gophian).
